#include <iostream>
#include <string>
#include "tree.h"
#include<fstream>
#include<istream>
#include<sstream>
#include <list>
using namespace std;

int main()
{
	string line,tmp;
	int count = 1;
	list<string> list;
	Tree<string> mytree;
	ifstream text ("example.txt");

		while (getline(text, line)){
			list.push_back(line);
		}
		while (list.empty()==false)
		{	

			stringstream ss;
			ss << list.front();
			list.pop_front();
			
			while (ss >> tmp)
			{
				mytree.insert(tmp, count);
			}
			count++;
		}
		mytree.inorder();	
	
}